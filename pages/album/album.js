// pages/album/album.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    choosed:false,
    pics:[]
  },

  //选择图片
  choosepic:function(){
    var that = this
    wx.chooseImage({
      count: 9,
      sourceType:['album'],
      sizeType:['original'],
      success:function(res){
        console.log(res)
        that.setData({
          pics:res.tempFilePaths,
          choosed:true
        })
      }
    })
  },

  //预览图片
  previewpic:function(e){
    var path = e.currentTarget.dataset.path
    var urls = []
    urls.push(path)
    wx.previewImage({
      urls: urls,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})