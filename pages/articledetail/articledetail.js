// pages/articledetail/articledetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    article: {}
  },

  /**
   * 收藏文章
   */
  save: function () {
    var article = this.data.article
    var node = {}
    node.title = article.title
    node.article = article
    node.time = new Date().getTime()
    var list = []
    wx.getStorage({
      key: 'articles',
      success: function (res) {
        list = res.data
        var flag = true
        for (let i = 0; i < list.length; i++) {
          if (list[i].title == node.title) {
            flag = false
            wx.showToast({
              title: '此前已收藏过',
            })
            break
          }
        }
        if (flag) {
          list.push(node)
          wx.setStorage({
            data: list,
            key: 'articles',
            success: function () {
              wx.showToast({
                title: '收藏成功',
              })
            }
          })
        }
      },
      fail: function () {
        list.push(node)
        wx.setStorage({
          data: list,
          key: 'articles',
          success: function () {
            wx.showToast({
              title: '收藏成功',
            })
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id
    var that = this
    var url = 'http://localhost:8080/api/article/' + id
    wx.request({
      url: url,
      success: function (res) {
        that.setData({
          article: res.data.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})