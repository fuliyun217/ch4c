// pages/downloading/downloading.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isDown:false,
    totalBytesExpectedToWrite:0,
    totalBytesWritten:0,
    progress:0
  },

  //下载文件
  download:function(){
    var that = this
    var url = "https://qiniu-ecdn.dcloud.net.cn/download/HBuilderX.3.99.2023122611.zip"
    const downTask = wx.downloadFile({
      url: url,
      success:function(res){
        console.log(res)
      }
    })
    downTask.onProgressUpdate(function(res){
      console.log(res)
      that.setData({
        totalBytesExpectedToWrite:res.totalBytesExpectedToWrite,
        totalBytesWritten: res.totalBytesWritten,
        progress: res.progress,
        isDown:true
      })
      if(res.progress>33){
        // downTask.abort()
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})