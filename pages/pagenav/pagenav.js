// pages/pagenav/pagenav.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    i:0,
    traffics:[
      {
        name:"bike",
        title:"自行车"
      },{
        name:"bus",
        title:"公交车"
      },{
        name:"subway",
        title:"地铁"
      },{
        name:"rocket",
        title:"火箭"
      }
    ]
  },

  //处理picker选择事件
  picker:function(e){
    console.log(e)
    this.setData({
      i:e.detail.value
    })
  },

  //处理表单提交数据
  pagetrip:function(e){
    console.log(e)
    var name = e.detail.value.name
    var dest = e.detail.value.dest
    var i = e.detail.value.traffic
    var traffics = this.data.traffics
    var url = '../pagetrip/pagetrip'
    url += "?name=" + name
    url += "&dest=" + dest
    url += "&traffic="+traffics[i].title
    wx.navigateTo({
      url: url
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})