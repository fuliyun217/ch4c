// index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pages:[
      "pages/pagenav/pagenav",
      "pages/article/article",
      "pages/album/album",
      "pages/downloading/downloading",
      "pages/whereami/whereami",
      "pages/sloper/sloper",
      "pages/compass/compass",
      "pages/doodle/doodle",
    ],
    menuitems:[
      {
        title:'跨页旅行',
        info:'页面路由API'
      },{
        title:'文章列表/收藏',
        info:'服务器/交互/缓存API'
      },{
        title:'精选相册',
        info:'图片处理API'
      },{
        title:'下载进度条',
        info:'文件上传下载API'
      },{
        title:'我在哪儿？',
        info:'位置信息API'
      },{
        title:'坡度计',
        info:'加速度计API'
      },{
        title:'指南针',
        info:'罗盘API'
      },{
        title:'音乐涂鸦',
        info:'画布和音乐API'
      }
    ]
  },

  /**
   * 链接到新页面
   */
  navto: function (e) {
    var index = e.currentTarget.dataset.index
    var pages = this.data.pages
    var url = pages[index]
    wx.navigateTo({
      url: "/"+url,

    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})