// pages/article/article.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    current:0,
    isNull:true,
    typelist:[],
    list:[],
    articles:[]
  },

  /**
   * 菜单切换
   */
  chBtn:function(e){
    var current = e.target.dataset.current
    this.setData({
      current:current
    })
  },

  /**
   * swiper切换
   */
  chSwiper:function(e){
    var current = e.detail.current
    this.setData({
      current:current
    })
  },

  /**
   * 查看文章详情
   */
  showdetail:function(e){
    var id = e.target.dataset.id
    wx.navigateTo({
      url: '../articledetail/articledetail?id='+id,
    })
  },

  /**
   * 查看收藏详情
   */
  showCollect:function(e){
    var index = e.target.dataset.index
    wx.navigateTo({
      url: '../collectdetail/collectdetail?index='+index,
    })
  },

  /**
   * 删除收藏
   */
  rmCollect:function(e){
    var that = this
    var index = e.target.dataset.index
    var coList = this.data.articles
    wx.showModal({
      title:'删除收藏',
      content:'确定要删除这条收藏吗？',
      cancelColor: '#f00',
      success:function(res){
        console.log(res)
        if(res.confirm){
          coList.splice(index,1)
          //console.log(coList)
          wx.setStorage({
            data: coList,
            key: 'articles',
            success:function(){
              that.setData({
                articles:coList
              })
            }
          })
          
        }
      }
    })
  },

   /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var url = 'http://localhost:8080/api/article'
    wx.request({
      url: url,
      method: 'GET',
      success: function(res){
        console.log(res)
        var list = res.data.data
        that.setData({
          list:list
        })
      }
    })
    wx.getStorage({
      key: 'articles',
      success:function(res){
        that.setData({
          articles:res.data,
          isNull:false
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    wx.getStorage({
      key: 'articles',
      success:function(res){
        that.setData({
          articles:res.data,
          isNull:false
        })
      },
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})